import { render, screen, waitFor } from '@testing-library/angular';
import { SignUpComponent } from './sign-up.component';
import userEvent from '@testing-library/user-event/';
import { HttpClientModule } from '@angular/common/http';
import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

type UniqueEmailCheck = {
  email: string,
}


let requestBody: any;
let counter = 0;
const server = setupServer(
  rest.post('/api/1.0/users', (req, res, ctx) => {
    requestBody = req.body;
    counter += 1;
    if(requestBody.email === 'non-unique@email.com'){
      return res(ctx.status(400), ctx.json({
        validationErrors: {email: 'E-mail in use'},
      }));
    }
    return res(ctx.status(200), ctx.json({}));
  }),
  rest.post('/api/1.0/user/email', (req, res, ctx) => {
    if ((req.body as UniqueEmailCheck).email === 'non-unique@email.com') {
      return res(ctx.status(200), ctx.json({}));
    }
    return res(ctx.status(404), ctx.json({}));
  }),
);

beforeEach(() => {
  counter = 0;
  server.resetHandlers();
});

beforeAll(() => server.listen());

afterAll(() => server.close());

const setup = async () => await render(SignUpComponent,
  {
    imports: [HttpClientModule, SharedModule, ReactiveFormsModule],
  });

describe('SignUpComponent', () => {
  describe('Layout', () => {
    it('has Sign Up header', async () => {
      await setup();
      const header = screen.getByRole('heading', {name: 'Sign Up'});
      expect(header).toBeInTheDocument();
    });

    it('has Username input', async () => {
      await setup();
      expect(screen.getByLabelText('Username')).toBeInTheDocument();
    });

    it('has Email input', async () => {
      await setup();
      expect(screen.getByLabelText('E-mail')).toBeInTheDocument();
    });

    it('has Password input', async () => {
      await setup();
      expect(screen.getByLabelText('Password')).toBeInTheDocument();
    });

    it('has password type for password input', async () => {
      await setup();
      expect(screen.getByLabelText('Password')).toHaveAttribute('type', 'password');
    });

    it('has Repeat Password input', async () => {
      await setup();
      expect(screen.getByLabelText('Password Repeat')).toBeInTheDocument();
    });

    it('has password type for Repeat password input', async () => {
      await setup();
      expect(screen.getByLabelText('Password Repeat')).toHaveAttribute('type', 'password');
    });

    it('has Sign Up button', async () => {
      await setup();
      expect(screen.getByRole('button', {name: 'Sign Up'})).toBeInTheDocument();
    });

    it('disables the button initially', async () => {
      await setup();
      expect(screen.getByRole('button', {name: 'Sign Up'})).toBeDisabled();
    });
  });
  describe('Interactions', () => {
    let button: any;
    const setupForm = async (values?: {email: string}) => {
      await setup();
      const password = screen.getByLabelText('Password');
      const email = screen.getByLabelText('E-mail');
      const username = screen.getByLabelText('Username');
      const passwordRepeat = screen.getByLabelText('Password Repeat');
      button = screen.getByRole('button', {name: 'Sign Up'});
      await userEvent.type(password, 'Password123');
      await userEvent.type(passwordRepeat, 'Password123');
      await userEvent.type(email, values?.email || 'user1@mail.com');
      await userEvent.type(username, 'user1');
    };

    it('enables the button when all the fields have valid input', async () => {
      await setupForm();
      expect(button).toBeEnabled();
    });

    it('sends username, email and password to backend after click Sign Up button', async () => {
      await setupForm();
      await userEvent.click(button);
      await waitFor(() => {
        expect(requestBody).toEqual({
          username: 'user1',
          email: 'user1@mail.com',
          password: 'Password123',
        });
      });
    });

    it('disables button when there is an ongoing api call', async () => {
      await setupForm();
      await userEvent.click(button);
      await userEvent.click(button);
      await waitFor(() => {
        expect(counter).toBe(1);
      });
    });

    it('displays spinner after clicking the submit', async () => {
      await setupForm();
      expect(screen.queryByRole('status')).not.toBeInTheDocument();
      await userEvent.click(button);
      expect(screen.queryByRole('status')).toBeInTheDocument();
    });

    it('displays account activation notification after successful sign up request', async () => {
      await setupForm();
      expect(screen.queryByText('Please check your e-mail to activate your account')).not.toBeInTheDocument();
      await userEvent.click(button);
      const text = await screen.findByText('Please check your e-mail to activate your account');
      expect(text).toBeInTheDocument();
    });

    it('hide signup form after successful sign up request', async () => {
      await setupForm();
      const form = screen.queryByTestId('form-sign-up');
      await userEvent.click(button);
      await screen.findByText('Please check your e-mail to activate your account');
      expect(form).not.toBeInTheDocument();
    });

    it('Displays validation error coming from backend after submit failure', async () => {
      await setupForm({email: 'non-unique@email.com'});
      await userEvent.click(button);
      const errorMessage = await screen.findByText('E-mail in use');
      expect(errorMessage).toBeInTheDocument();
      expect(screen.queryByRole('status')).not.toBeInTheDocument();
    });

    it('Hide spinner after sign up request fail', async() => {
      await setupForm({email: 'non-unique@email.com'});
      await userEvent.click(button);
      await screen.findByText('E-mail in use');
      expect(screen.queryByRole('status')).not.toBeInTheDocument();
    })
  });

  describe('Validation', () => {

    it.each`
    label               | inputValue                | message
    ${'Username'}       | ${'{space}{backspace}'}   | ${'Username is required'}
    ${'Username'}       | ${'123'}                  | ${'Username must be at least 4 characters long'}
    ${'E-mail'}         | ${'{space}{backspace}'}   | ${'E-mail is required'}
    ${'E-mail'}         | ${'wrong-format'}         | ${'Invalid E-mail'}
    ${'Password'}       | ${'password'}             | ${'Password must have at least 1 uppercase, 1 lowercase letter and 1 number'}
    ${'Password'}       | ${'PASSWORD'}             | ${'Password must have at least 1 uppercase, 1 lowercase letter and 1 number'}
    ${'Password'}       | ${'password'}             | ${'Password must have at least 1 uppercase, 1 lowercase letter and 1 number'}
    ${'Password'}       | ${'Password'}             | ${'Password must have at least 1 uppercase, 1 lowercase letter and 1 number'}
    ${'Password'}       | ${'{space}{backspace}'}   | ${'Password is required'}
    ${'Password Repeat'} | ${'pass'}                | ${'Password mismatch'}
    ${'E-mail'}         | ${'non-unique@email.com'} | ${'E-mail in use'}
    `('Displays $message when $label has "$value"', async ({label, inputValue, message}) => {
      await setup();
      expect(screen.queryByText(message)).not.toBeInTheDocument();
      const input = screen.getByLabelText(label);
      await userEvent.type(input, inputValue);
      await userEvent.tab();
      const errorMessage = await screen.findByText(message);
      expect(errorMessage).toBeInTheDocument();
    });

  });
});
