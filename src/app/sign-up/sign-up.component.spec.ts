import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpComponent } from './sign-up.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignUpComponent],
      imports: [HttpClientTestingModule, SharedModule, ReactiveFormsModule],
    })
      .compileComponents();

    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Layout', () => {
    it('has Sign-up header', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const h1 = signUp.querySelector('h1');
      expect(h1?.textContent).toBe('Sign Up');
    });

    it('has Username input', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const label = signUp.querySelector('label[for="username"]');
      const input = signUp.querySelector('input[id="username"]');
      expect(input).toBeTruthy();
      expect(label).toBeTruthy();
      expect(label?.textContent).toBe('Username');
    });

    it('has E-mail input', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const label = signUp.querySelector('label[for="email"]');
      const input = signUp.querySelector('input[id="email"]');
      expect(input).toBeTruthy();
      expect(label).toBeTruthy();
      expect(label?.textContent).toBe('E-mail');
    });

    it('has password input', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const label = signUp.querySelector('label[for="password"]');
      const input = signUp.querySelector('input[id="password"]');
      expect(input).toBeTruthy();
      expect(label).toBeTruthy();
      expect(label?.textContent).toBe('Password');
    });

    it('has password type for password input', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const input = signUp.querySelector('input[id="password"]') as HTMLInputElement;
      expect(input?.type).toBe('password');
    });

    it('has password repeat input', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const label = signUp.querySelector('label[for="password"]');
      const input = signUp.querySelector('input[id="password"]');
      expect(input).toBeTruthy();
      expect(label).toBeTruthy();
      expect(label?.textContent).toBe('Password');
    });

    it('has password type for password repeat input', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const input = signUp.querySelector('input[id="passwordRepeat"]') as HTMLInputElement;
      expect(input?.type).toBe('password');
    });

    it('has Sign-up button', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const button = signUp.querySelector('button');
      expect(button?.textContent).toContain('Sign Up');
    });

    it('disables the button initially', () => {
      const signUp = fixture.nativeElement as HTMLElement;
      const button = signUp.querySelector('button');
      expect(button?.disabled).toBeTruthy();
    });

  });

  describe('Interactions', () => {
    let button: any;
    let httpTestingController: HttpTestingController;
    let signUp: HTMLElement;

    const setupForm = async () => {
      httpTestingController = TestBed.inject(HttpTestingController);
      signUp = fixture.nativeElement;
      const passwordInput = signUp.querySelector('input[id="password"]') as HTMLInputElement;
      const passwordRepeatInput = signUp.querySelector('input[id="passwordRepeat"]') as HTMLInputElement;
      const emailInput = signUp.querySelector('input[id="email"]') as HTMLInputElement;
      const usernameInput = signUp.querySelector('input[id="username"]') as HTMLInputElement;
      passwordInput.value = 'Password123';
      passwordInput.dispatchEvent(new Event('input'));
      passwordRepeatInput.value = 'Password123';
      passwordRepeatInput.dispatchEvent(new Event('input'));
      usernameInput.value = 'user1';
      usernameInput.dispatchEvent(new Event('input'));
      emailInput.value = 'user1@mail.com';
      emailInput.dispatchEvent(new Event('input'));
      emailInput.dispatchEvent(new Event('blur'));
      fixture.detectChanges();
      button = signUp.querySelector('button');
    };

    it('enables the button when all the fields have valid input', async () => {
      await setupForm();
      expect(button?.disabled).toBeFalsy();
    });

    it('sends username, email and password to backend after click Sign Up button', async () => {
      await setupForm();
      button?.click();
      const req = httpTestingController.expectOne('/api/1.0/users');
      const requestBody = req.request.body;
      expect(requestBody).toEqual({
        username: 'user1',
        email: 'user1@mail.com',
        password: 'Password123',
      });

    });

    it('disables button when there is an ongoing api call', async () => {
      await setupForm();
      button?.click();
      fixture.detectChanges();
      button?.click();
      httpTestingController.expectOne('/api/1.0/users');
      expect(button?.disabled).toBeTruthy();
    });

    it('displays spinner after clicking the submit', async () => {
      await setupForm();
      expect(signUp.querySelector('span[role="status"]')).toBeFalsy();
      button?.click();
      fixture.detectChanges();
      expect(signUp.querySelector('span[role="status"]')).toBeTruthy();
    });

    it('displays account activation notification after successful sign up request', async () => {
      await setupForm();
      expect(signUp.querySelector('.alert-success')).toBeFalsy();
      button?.click();
      const req = httpTestingController.expectOne('/api/1.0/users');
      req.flush({});
      fixture.detectChanges();
      const message = signUp.querySelector('.alert-success');
      expect(message?.textContent)
        .toContain('Please check your e-mail to activate your account');
    });

    it('hide signup form after successful sign up request', async () => {
      await setupForm();
      expect(signUp.querySelector('div[data-testid="form-sign-up"]')).toBeTruthy();
      button?.click();
      const req = httpTestingController.expectOne('/api/1.0/users');
      req.flush({});
      fixture.detectChanges();
      expect(signUp.querySelector('div[data-testid="form-sign-up"]')).toBeFalsy();
    });

    it('Displays validation error coming from backend after submit failure', async () => {
      await setupForm();
      button?.click();
      const req = httpTestingController.expectOne('/api/1.0/users');
      req.flush({
        validationErrors: {email: 'E-mail in use'}
      }, {
        status: 400,
        statusText: 'Bad Request',
      });
      fixture.detectChanges();
      const validationElement = signUp.querySelector(`div[data-testid="email-validation"]`);
      expect(validationElement?.textContent).toContain('E-mail in use');
    });

    it('Hide spinner after sign up request fail', async() => {
      await setupForm();
      button?.click();
      const req = httpTestingController.expectOne('/api/1.0/users');
      req.flush({
        validationErrors: {email: 'E-mail in use'}
      }, {
        status: 400,
        statusText: 'Bad Request',
      });
      fixture.detectChanges();
      expect(signUp.querySelector('span[role="status"]')).toBeFalsy();

    })
  });

  describe('Validation', () => {
    const testCases = [
      {field: 'username', value: '', error: 'Username is required'},
      {field: 'username', value: '123', error: 'Username must be at least 4 characters long'},
      {field: 'email', value: '', error: 'E-mail is required'},
      {field: 'email', value: 'invalid-format', error: 'Invalid E-mail'},
      {field: 'password', value: '', error: 'Password is required'},
      {
        field: 'password',
        value: 'password',
        error: 'Password must have at least 1 uppercase, 1 lowercase letter and 1 number',
      },
      {
        field: 'password',
        value: 'PASSWORD',
        error: 'Password must have at least 1 uppercase, 1 lowercase letter and 1 number',
      },
      {
        field: 'password',
        value: 'Password',
        error: 'Password must have at least 1 uppercase, 1 lowercase letter and 1 number',
      },
      {
        field: 'password',
        value: 'password123',
        error: 'Password must have at least 1 uppercase, 1 lowercase letter and 1 number',
      },
      {field: 'passwordRepeat', value: 'pass', error: 'Password mismatch'},

    ];

    testCases.forEach(({field, value, error}) => {
      it(`Displays ${error} when ${field} has '${value}'`, () => {
        const signUp = fixture.nativeElement as HTMLElement;
        expect(signUp.querySelector(`div[data-testid="${field}-validation"]`)).toBeNull();
        const input = signUp.querySelector(`input[id=${field}]`) as HTMLInputElement;
        input.value = value;
        input.dispatchEvent(new Event('input'));
        input.dispatchEvent(new Event('blur'));
        fixture.detectChanges();
        const validationElement = signUp.querySelector(`div[data-testid="${field}-validation"]`);
        expect(validationElement?.textContent).toContain(error);
      });
    });

    it('Displays E-mail in use when E-mail is not unique', () => {
      let httpTestingController = TestBed.inject(HttpTestingController);
      const signUp = fixture.nativeElement as HTMLElement;
      expect(signUp.querySelector(`div[data-testid="email-validation"]`)).toBeNull();
      const input = signUp.querySelector(`input[id="email"]`) as HTMLInputElement;
      input.value = 'non-unique@email.com';
      input.dispatchEvent(new Event('input'));
      input.dispatchEvent(new Event('blur'));
      const request = httpTestingController.expectOne(({url, method, body}) => {
        if (url === '/api/1.0/user/email' && method === 'POST') {
          return body.email === 'non-unique@email.com';
        }
        return false;
      });
      request.flush({});
      fixture.detectChanges();
      const validationElement = signUp.querySelector(`div[data-testid="email-validation"]`);
      expect(validationElement?.textContent).toContain('E-mail in use');
    });
  });
});

